import requests
import pydantic
import json
from typing import Optional
from typing import List
from pydantic import BaseModel, ValidationError, Field


class Coord(BaseModel):
    lon: float
    lat: float

class Weather(BaseModel):
    id: int
    main: str
    description: str
    icon: str

class Main(BaseModel):
    temp: float
    feels_like: float
    temp_min: float
    temp_max: float
    pressure: int
    humidity: int

class Wind(BaseModel):
    speed: int
    deg: int

class Rain(BaseModel):
    h: Optional[float] = None

class Clouds(BaseModel):
    all: int

class Sys(BaseModel):
    type: int
    id: int
    country: str
    sunrise: int
    sunset: int

class Root(BaseModel):
    coord: Coord
    weather: List[Weather]
    base: str
    main: Main
    visibility: int
    wind: Wind
    rain: Optional[Rain] = None
    clouds: Clouds
    dt: int
    sys: Sys
    timezone: int
    id: int
    name: str
    cod: int


response = requests.get("http://api.openweathermap.org/data/2.5/weather?id=524901&appid=3896a2ea2a68e3025ac9fe9e6aff229d")
print(response.content)

info = Root.parse_raw( response.content )


print(info)

